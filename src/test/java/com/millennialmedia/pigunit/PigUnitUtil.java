package com.millennialmedia.pigunit;

import java.io.IOException;

import org.apache.pig.ExecType;
import org.apache.pig.PigException;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.pigunit.Cluster;
import org.apache.pig.pigunit.PigTest;
import org.apache.pig.pigunit.pig.PigServer;

/**
 * Utility class to create a PigTest instance.
 * 
 * @author Mathieu Dumoulin
 */
public final class PigUnitUtil {
   public static PigTest createPigTest(String scriptFile, String[] inputs) throws PigException {
      try {
         final PigServer pigServer = new PigServer(ExecType.LOCAL);
         final Cluster pigCluster = new Cluster(pigServer.getPigContext());
         return new PigTest(scriptFile, inputs, pigServer, pigCluster);
      } catch (final ExecException e) {
         throw new PigException("PigServer: Failed to create PigTest instance. ", e);
      } catch (final IOException e) {
         throw new PigException("PigTest: Failed to create PigTest instance. ", e);
      }
   }

   private PigUnitUtil() {
   }
}