package ca.ulaval.ift.alphonse.filtering;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;

import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.junit.Test;

public class StopWordFilterTest {
   private static final TupleFactory TUPLE_FACTORY = TupleFactory.getInstance();

   private static final String STOPWORD = "this";
   private static final String SOME_WORD_THAT_ISNT_A_STOPWORD = "word";

   private final StopWordFilter filterUDF = new StopWordFilter();

   @Test
   public void givenLineOfTextWithStopWordsShouldFilterShouldReturnTrue() throws IOException {
      final Tuple lineOfText = TUPLE_FACTORY.newTuple(1);
      lineOfText.set(0, STOPWORD);

      final Boolean filterResult = filterUDF.exec(lineOfText);

      assertThat(filterResult, equalTo(true));
   }

   @Test
   public void givenLineOfTextWithNoStopWordsShouldFilterShouldReturnFalse() throws IOException {
      final Tuple lineOfText = TUPLE_FACTORY.newTuple(1);
      lineOfText.set(0, SOME_WORD_THAT_ISNT_A_STOPWORD);

      final boolean filterResult = filterUDF.exec(lineOfText);

      assertThat(filterResult, equalTo(false));
   }

   @Test(expected = IOException.class)
   public void givenTupleWithSizeTwoShouldThrowException() throws IOException {
      final Tuple lineOfText = TUPLE_FACTORY.newTuple(2);

      filterUDF.exec(lineOfText);
   }

   @Test(expected = IOException.class)
   public void givenNullTupleShouldThrowException() throws IOException {
      filterUDF.exec(null);
   }
}
