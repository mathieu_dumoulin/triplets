package ca.ulaval.ift.alphonse.evaluation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.util.Utils;
import org.apache.pig.parser.ParserException;
import org.junit.Test;

/**
 * JUnit tests for SimpleStopWordTokenizer.
 * 
 * @author Mathieu Dumoulin
 */
public class SimpleStopWordTokenizerTest {
   private static final String ONE_CHAR_WORD = "e";

   private static final String WORD_WITH_NON_ALPHANUMERIC = "$WORD";

   private static TupleFactory tupleFactory = TupleFactory.getInstance();

   private static final String LINE_OF_TEXT = "this be a text with some stop words";
   private static final String TEXT_NO_STOPWORDS = "this text with some stop words";

   private static final String[] EXPECTED_TOKENS = { "text", "some", "stop", "words" };

   private final SimpleStopWordTokenizer tokenizerUDF = new SimpleStopWordTokenizer();
   private static final Tuple LINE_OF_TEXT_TUPLE = tupleFactory.newTuple(LINE_OF_TEXT);

   @Test
   public void givenTupleContainingTextShouldReturnDataBag() throws IOException {
      final DataBag bag = tokenizerUDF.exec(LINE_OF_TEXT_TUPLE);

      assertThat(bag, notNullValue());
   }

   @Test
   public void givenTupleWithNullFieldShouldReturnNull() throws IOException {
      final Tuple input = tupleFactory.newTuple(1);
      input.set(0, null);
      final DataBag bag = tokenizerUDF.exec(input);

      assertThat(bag, nullValue());
   }

   @Test(expected = IOException.class)
   public void givenEmptyTupleExecShouldThrowIOException() throws IOException {
      final Tuple input = tupleFactory.newTuple(0);
      tokenizerUDF.exec(input);
   }

   @Test(expected = IOException.class)
   public void givenTupleWithMoreThanOneFieldExecShouldThrowIOException() throws IOException {
      final Tuple input = tupleFactory.newTuple(Arrays.asList("one", "two"));
      tokenizerUDF.exec(input);
   }

   @Test
   public void givenTextWithNoStopWordsShouldReturnBagOfTokenizedTextWithAllWords() throws IOException {
      final Tuple input = tupleFactory.newTuple(TEXT_NO_STOPWORDS);
      final DataBag bag = tokenizerUDF.exec(input);

      final List<String> tokens = new ArrayList<String>();
      for (final Tuple tuple : bag) {
         tokens.add((String) tuple.get(0));
      }

      assertThat(EXPECTED_TOKENS, arrayContaining(tokens.toArray()));
   }

   @Test
   public void givenTextShouldNotOutputStopwords() throws IOException {
      final DataBag bag = tokenizerUDF.exec(LINE_OF_TEXT_TUPLE);

      final List<String> tokens = new ArrayList<String>();
      for (final Tuple tuple : bag) {
         tokens.add((String) tuple.get(0));
      }

      assertThat(EXPECTED_TOKENS, arrayContaining(tokens.toArray()));
   }

   @Test
   public void shouldNotOutputWordsWithNonAlphanumericCharacters() throws IOException {
      final String line = LINE_OF_TEXT + " " + WORD_WITH_NON_ALPHANUMERIC;

      final Tuple input = tupleFactory.newTuple(line);
      final DataBag bag = tokenizerUDF.exec(input);

      final List<String> tokens = new ArrayList<String>();
      for (final Tuple tuple : bag) {
         tokens.add((String) tuple.get(0));
      }

      assertThat(EXPECTED_TOKENS, arrayContaining(tokens.toArray()));
   }

   @Test
   public void shouldNotOutputWordsWithLenghtOfOne() throws IOException {
      final String line = LINE_OF_TEXT + " " + ONE_CHAR_WORD;

      final Tuple input = tupleFactory.newTuple(line);
      final DataBag bag = tokenizerUDF.exec(input);

      final List<String> tokens = new ArrayList<String>();
      for (final Tuple tuple : bag) {
         tokens.add((String) tuple.get(0));
      }

      assertThat(EXPECTED_TOKENS, arrayContaining(tokens.toArray()));
   }

   @Test
   public void outputSchemaShouldBeBagOfStringTuples() throws ParserException {
      final Schema input = Utils.getSchemaFromString("w: chararray");
      final Schema outputSchema = tokenizerUDF.outputSchema(input);

      assertThat(outputSchema, notNullValue());
   }
}
