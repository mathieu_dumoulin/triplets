package ca.ulaval.ift.alphonse.evaluation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.junit.Before;
import org.junit.Test;

/**
 * JUnit tests for CosineSimilarity.
 * 
 * @author Mathieu Dumoulin
 */
public class CosineSimilarityTest {
   private static final String TEST_RESOURCES_FOLDER = "src/test/resources/";

   private static final double ZERO_SCORE = 0.0;
   private static final TupleFactory TUPLE_FACTORY = TupleFactory.getInstance();
   private static final BagFactory BAG_FACTORY = BagFactory.getInstance();

   private static final double EPSILON = 0.0000001;
   private static final double OVERFLOW_TEST_ANSWER = 0.041154738;

   private static final int A_GIVEN_INTERSECTION_SIZE_1 = 5;
   private static final int ANOTHER_GIVEN_SIZE_1 = 25;
   private static final int A_GIVEN_SIZE_1 = 18;

   private DataBag relation1;
   private DataBag relation2;
   private DataBag emptyRelation;

   private final Long[] relation1TupleValues = { 1L, 2L, 3L };
   private final Long[] relation2TupleValues = { 2L, 3L, 4L };
   private final Long[] emptyRelationValues = {};

   private static final int RELATIONS_1_AND_2_INTER_SIZE = 2;
   private final CosineSimilarity cosineUDF = new CosineSimilarity();

   @Before
   public void setUp() throws Exception {
      relation1 = BAG_FACTORY.newDefaultBag(createListOfTuplesFromArray(relation1TupleValues));
      relation2 = BAG_FACTORY.newDefaultBag(createListOfTuplesFromArray(relation2TupleValues));
      emptyRelation = BAG_FACTORY.newDefaultBag(createListOfTuplesFromArray(emptyRelationValues));
   }

   private <T> List<Tuple> createListOfTuplesFromArray(final T[] array) {
      final List<Tuple> tuples = new ArrayList<Tuple>();
      for (final T value : array) {
         tuples.add(TUPLE_FACTORY.newTuple(value));
      }
      return tuples;
   }

   // test that the method cosine works as intended
   @Test
   public void givenTestInput1ComputeCosineCorrectly() {
      final int sizeA = A_GIVEN_SIZE_1;
      final int sizeB = ANOTHER_GIVEN_SIZE_1;
      final int sizeInter = A_GIVEN_INTERSECTION_SIZE_1;

      final double expectedCosine = cosine(sizeA, sizeB, sizeInter);

      final double computedCosine = cosineUDF.cosine(sizeA, sizeB, sizeInter);

      assertThat(expectedCosine, is(closeTo(computedCosine, EPSILON)));
   }

   private double cosine(int sizeA, int sizeB, int sizeInter) {
      return sizeInter / Math.sqrt(sizeA * sizeB);
   }

   @Test
   public void givenTwoBagsShouldReturnCosineValue() throws IOException {
      final Tuple input = TUPLE_FACTORY.newTuple();
      input.append(relation1);
      input.append(relation2);
      final double expectedScore = cosine(relation1TupleValues.length, relation2TupleValues.length,
            RELATIONS_1_AND_2_INTER_SIZE);

      final double computedScore = cosineUDF.exec(input);

      assertThat(expectedScore, is(closeTo(computedScore, EPSILON)));
   }

   @Test
   public void whenFirstBagIsEmptyCosineShouldBeZero() throws IOException {
      final Tuple input = TUPLE_FACTORY.newTuple();
      input.append(relation1);
      input.append(emptyRelation);

      final double computedScore = cosineUDF.exec(input);

      assertThat(ZERO_SCORE, is(closeTo(computedScore, EPSILON)));
   }

   @Test
   public void whenSecondBagIsEmptyCosineShouldBeZero() throws IOException {
      final Tuple input = TUPLE_FACTORY.newTuple();
      input.append(emptyRelation);
      input.append(relation2);

      final double computedScore = cosineUDF.exec(input);

      assertThat(ZERO_SCORE, is(closeTo(computedScore, EPSILON)));
   }

   private Long[] readRelationToLongArray(File file) throws IOException {
      BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

      List<Long> array = new LinkedList<Long>();
      String line;
      while ((line = br.readLine()) != null) {
         array.add(Long.parseLong(line));
      }

      br.close();

      return array.toArray(new Long[0]);
   }

   @Test
   public void testWeirdCase() throws IOException {
      File beContextVectorFile = new File(TEST_RESOURCES_FOLDER + "Be_context-vector.txt");
      File voidContextVectorFile = new File(TEST_RESOURCES_FOLDER + "Void_context-vector.txt");

      Long[] beContextVectorTupleValues = readRelationToLongArray(beContextVectorFile);
      Long[] voidContextVectorTupleValues = readRelationToLongArray(voidContextVectorFile);

      DataBag beRelation = BAG_FACTORY.newDefaultBag(createListOfTuplesFromArray(beContextVectorTupleValues));
      DataBag voidRelation = BAG_FACTORY.newDefaultBag(createListOfTuplesFromArray(voidContextVectorTupleValues));

      final Tuple input1 = TUPLE_FACTORY.newTuple();
      input1.append(voidRelation);
      input1.append(beRelation);
      final Tuple input2 = TUPLE_FACTORY.newTuple();
      input2.append(beRelation);
      input2.append(beRelation);
      final Tuple input3 = TUPLE_FACTORY.newTuple();
      input3.append(voidRelation);
      input3.append(voidRelation);

      Date start1 = new Date();
      final double computedScore1 = cosineUDF.exec(input1);
      Date end1 = new Date();
      Date start2 = new Date();
      final double computedScore2 = cosineUDF.exec(input2);
      Date end2 = new Date();
      Date start3 = new Date();
      final double computedScore3 = cosineUDF.exec(input3);
      Date end3 = new Date();

      assertThat(computedScore1, is(closeTo(OVERFLOW_TEST_ANSWER, EPSILON)));
      assertThat(computedScore2, is(closeTo(1, EPSILON)));
      assertThat(computedScore3, is(closeTo(1, EPSILON)));

      System.out.println((end1.getTime() - start1.getTime()) + "; " + (end2.getTime() - start2.getTime()) + "; "
            + (end3.getTime() - start3.getTime()));
   }
}
