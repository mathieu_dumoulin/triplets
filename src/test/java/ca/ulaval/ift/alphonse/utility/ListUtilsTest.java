package ca.ulaval.ift.alphonse.utility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class ListUtilsTest {
   private static final List<Long> L11 = new LinkedList<Long>();
   private static final Long[] L11_ELEMENTS = new Long[] { 1L, 2L, 4L, 5L };
   private static final List<Long> L12 = new ArrayList<Long>();
   private static final Long[] L12_ELEMENTS = new Long[] { 2L, 3L, 6L, 7L };
   private static final List<Long> L13 = new ArrayList<Long>();
   private static final Long[] L13_ELEMENTS = new Long[] { 1L, 3L, 6L, 10L };
   private static final List<Long> L14 = new ArrayList<Long>();
   private static final Long[] L14_ELEMENTS = new Long[] { 4L, 5L, 7L };
   private static final List<Long> L15 = new ArrayList<Long>();
   private static final Long[] L15_ELEMENTS = new Long[] {};

   private static final List<Pair<Long, Double>> L21 = new LinkedList<Pair<Long, Double>>();
   private static final Long[] L21_ELEMENTS = new Long[] { 1L, 2L, 4L, 5L };
   private static final Double[] L21_WEIGHTS = new Double[] { 0.75, 1.2, 1.5, 2.0 };
   private static final List<Pair<Long, Double>> L22 = new ArrayList<Pair<Long, Double>>();
   private static final Long[] L22_ELEMENTS = new Long[] { 2L, 3L, 6L, 7L };
   private static final Double[] L22_WEIGHTS = new Double[] { 1.0, 1.0, 1.0, 1.0 };
   private static final List<Pair<Long, Double>> L23 = new ArrayList<Pair<Long, Double>>();
   private static final Long[] L23_ELEMENTS = new Long[] { 1L, 3L, 6L, 10L };
   private static final Double[] L23_WEIGHTS = new Double[] { 0.5, 1.2, 1.5, 2.0 };
   private static final List<Pair<Long, Double>> L24 = new ArrayList<Pair<Long, Double>>();
   private static final Long[] L24_ELEMENTS = new Long[] { 4L, 5L, 7L };
   private static final Double[] L24_WEIGHTS = new Double[] { 1.0, 1.0, 1.0 };
   private static final List<Pair<Long, Double>> L25 = new ArrayList<Pair<Long, Double>>();
   private static final Long[] L25_ELEMENTS = new Long[] {};
   private static final Double[] L25_WEIGHTS = new Double[] {};

   private static void fillListWith(List<Long> l, Long[] elements) {
      for (Long element : elements)
         l.add(element);
   }
   private static void fillWeightedListWith(List<Pair<Long, Double>> l, Long[] elements, Double[] weights) {
      if (elements.length == weights.length)
         for (int i = 0; i < elements.length; ++i)
            l.add(new Pair<Long, Double>(elements[i], weights[i]));
   }

   private static final double EPSILON = 0.0000001;

   @BeforeClass
   public static void classSetUp() {
      fillListWith(L11, L11_ELEMENTS);
      fillListWith(L12, L12_ELEMENTS);
      fillListWith(L13, L13_ELEMENTS);
      fillListWith(L14, L14_ELEMENTS);
      fillListWith(L15, L15_ELEMENTS);

      fillWeightedListWith(L21, L21_ELEMENTS, L21_WEIGHTS);
      fillWeightedListWith(L22, L22_ELEMENTS, L22_WEIGHTS);
      fillWeightedListWith(L23, L23_ELEMENTS, L23_WEIGHTS);
      fillWeightedListWith(L24, L24_ELEMENTS, L24_WEIGHTS);
      fillWeightedListWith(L25, L25_ELEMENTS, L25_WEIGHTS);
   }

   @Test
   public void testGetLongComparator() {
      Object c1 = ListUtils.getLongComparator();
      Object c2 = ListUtils.getLongComparator();

      assertTrue("Repetitive calls to ListUtils.getLongComparator() should have returned the same object", c1 == c2);
   }

   @Test
   public void testGetIntersectionSize() {
      assertEquals("L1 vs L1", 4L, ListUtils.getIntersectionSize(L11, L11, ListUtils.getLongComparator()));

      assertEquals("L1 vs L2", 1L, ListUtils.getIntersectionSize(L11, L12, ListUtils.getLongComparator()));
      assertEquals("L2 vs L1", 1L, ListUtils.getIntersectionSize(L12, L11, ListUtils.getLongComparator()));

      assertEquals("L1 vs L3", 1L, ListUtils.getIntersectionSize(L11, L13, ListUtils.getLongComparator()));
      assertEquals("L3 vs L1", 1L, ListUtils.getIntersectionSize(L13, L11, ListUtils.getLongComparator()));

      assertEquals("L2 vs L3", 2L, ListUtils.getIntersectionSize(L12, L13, ListUtils.getLongComparator()));
      assertEquals("L3 vs L2", 2L, ListUtils.getIntersectionSize(L13, L12, ListUtils.getLongComparator()));

      assertEquals("L1 vs L4", 2L, ListUtils.getIntersectionSize(L11, L14, ListUtils.getLongComparator()));
      assertEquals("L4 vs L1", 2L, ListUtils.getIntersectionSize(L14, L11, ListUtils.getLongComparator()));

      assertEquals("L2 vs L4", 1L, ListUtils.getIntersectionSize(L12, L14, ListUtils.getLongComparator()));
      assertEquals("L4 vs L2", 1L, ListUtils.getIntersectionSize(L14, L12, ListUtils.getLongComparator()));

      assertEquals("L3 vs L4", 0L, ListUtils.getIntersectionSize(L13, L14, ListUtils.getLongComparator()));
      assertEquals("L4 vs L3", 0L, ListUtils.getIntersectionSize(L14, L13, ListUtils.getLongComparator()));

      assertEquals("L5 vs L1", 0L, ListUtils.getIntersectionSize(L15, L11, ListUtils.getLongComparator()));
      assertEquals("L1 vs L5", 0L, ListUtils.getIntersectionSize(L11, L15, ListUtils.getLongComparator()));
   }

   @Test
   public void testGetWeightedListIntersectionSize() {
      assertEquals("L1 vs L1", 5.45,
            ListUtils.getWeightedListIntersectionSize(L21, L21, ListUtils.getLongComparator()), EPSILON);

      assertEquals("L1 vs L2", 1.0, ListUtils.getWeightedListIntersectionSize(L21, L22, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L2 vs L1", 1.0, ListUtils.getWeightedListIntersectionSize(L22, L21, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L1 vs L3", 0.5, ListUtils.getWeightedListIntersectionSize(L21, L23, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L3 vs L1", 0.5, ListUtils.getWeightedListIntersectionSize(L23, L21, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L2 vs L3", 2.0, ListUtils.getWeightedListIntersectionSize(L22, L23, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L3 vs L2", 2.0, ListUtils.getWeightedListIntersectionSize(L23, L22, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L1 vs L4", 2.0, ListUtils.getWeightedListIntersectionSize(L21, L24, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L4 vs L1", 2.0, ListUtils.getWeightedListIntersectionSize(L24, L21, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L2 vs L4", 1.0, ListUtils.getWeightedListIntersectionSize(L22, L24, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L4 vs L2", 1.0, ListUtils.getWeightedListIntersectionSize(L24, L22, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L3 vs L4", 0.0, ListUtils.getWeightedListIntersectionSize(L23, L24, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L4 vs L3", 0.0, ListUtils.getWeightedListIntersectionSize(L24, L23, ListUtils.getLongComparator()),
            EPSILON);

      assertEquals("L5 vs L1", 0.0, ListUtils.getWeightedListIntersectionSize(L25, L21, ListUtils.getLongComparator()),
            EPSILON);
      assertEquals("L1 vs L5", 0.0, ListUtils.getWeightedListIntersectionSize(L21, L25, ListUtils.getLongComparator()),
            EPSILON);
   }

   @Test
   public void testGetWeightedListSize() {
      assertEquals("L21 size", 5.45, ListUtils.getWeightedListSize(L21), EPSILON);
      assertEquals("L22 size", 4.0, ListUtils.getWeightedListSize(L22), EPSILON);
      assertEquals("L23 size", 5.2, ListUtils.getWeightedListSize(L23), EPSILON);
      assertEquals("L24 size", 3.0, ListUtils.getWeightedListSize(L24), EPSILON);
      assertEquals("L25 size", 0, ListUtils.getWeightedListSize(L25), EPSILON);
   }
}
