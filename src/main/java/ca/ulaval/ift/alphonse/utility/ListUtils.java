package ca.ulaval.ift.alphonse.utility;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ListUtils {
   private static final class LongComparator implements Comparator<Long> {
      @Override
      public int compare(Long o1, Long o2) {
         return o1.compareTo(o2);
      }
   }
   private static final Comparator<Long> LONG_COMPARATOR = new LongComparator();
   public static final Comparator<Long> getLongComparator() {
      return LONG_COMPARATOR;
   }

   private static final class WeightedLongComparator implements Comparator<Pair<Long, Double>> {
      @Override
      public int compare(Pair<Long, Double> o1, Pair<Long, Double> o2) {
         return o1.first.compareTo(o2.first);
      }
   }
   private static final Comparator<Pair<Long, Double>> WEIGHTED_LONG_COMPARATOR = new WeightedLongComparator();
   public static final Comparator<Pair<Long, Double>> getWeightedLongComparator() {
      return WEIGHTED_LONG_COMPARATOR;
   }

   /**
    * <p>
    * A method returning the size of the intersection between two lists without computing this intersection. The
    * algorithm complexity is in O(n) n being the greater list size. Also, the algorithm uses a constant amount of
    * memory excluding the space taken by the lists.
    * </p>
    * <p>
    * <b>IMPORTANT</b> : the lists must be sorted ascendantly for the algorithm to work properly.
    * </p>
    * 
    * @param list1
    *           an ascendantly sorted list
    * @param list2
    *           an ascendantly sorted list
    * @param comparator
    *           the comparator used to sort the lists
    * @return the size of the intersection of the two lists
    * @author Gabriel Massicotte
    */
   public static final <T> int getIntersectionSize(final List<T> list1, final List<T> list2,
         final Comparator<T> comparator) {
      int intersectionSize = 0;
      Iterator<T> relationIt = list1.iterator();
      Iterator<T> relationCandidateSetIt = list2.iterator();
      T relationId;
      T relationCandidateSetId;
      boolean keepGoing = relationIt.hasNext() && relationCandidateSetIt.hasNext();
      boolean relationItHasNext;
      boolean relationCandidateSetItHasNext;

      if (keepGoing) {

         relationId = relationIt.next();
         relationCandidateSetId = relationCandidateSetIt.next();

         while (keepGoing) {
            relationItHasNext = relationIt.hasNext();
            relationCandidateSetItHasNext = relationCandidateSetIt.hasNext();

            int compareResult = comparator.compare(relationId, relationCandidateSetId);
            if (compareResult == 0) {

               ++intersectionSize;
               keepGoing = relationItHasNext && relationCandidateSetItHasNext;

               if (keepGoing) {
                  relationId = relationIt.next();
                  relationCandidateSetId = relationCandidateSetIt.next();
               }

            } else if (relationItHasNext && compareResult == -1) {
               relationId = relationIt.next();
            } else if (relationCandidateSetItHasNext) {
               relationCandidateSetId = relationCandidateSetIt.next();
            } else {
               keepGoing = false;
            }

         }
      }

      return intersectionSize;
   }

   /**
    * <p>
    * A method returning the size of the pseudo intersection between two weighted lists without computing this
    * intersection. When items intersect, the intersection size is incremented by the minimum weight instead 1. The
    * algorithm complexity is in O(n), n being the greater list size. Also, the algorithm uses a constant amount of
    * memory excluding the space taken by the lists.
    * </p>
    * <p>
    * <b>IMPORTANT</b> : the lists must be sorted ascendantly by the value of type T for the algorithm to work properly.
    * </p>
    * 
    * @param list1
    *           an ascendantly sorted list
    * @param list2
    *           an ascendantly sorted list
    * @param comparator
    *           the comparator used to sort the lists
    * @return the size of the intersection of the two lists
    * @author Gabriel Massicotte
    */
   public static final <T> double getWeightedListIntersectionSize(final List<Pair<T, Double>> list1,
         final List<Pair<T, Double>> list2, final Comparator<T> comparator) {
      double intersectionSize = 0;
      Iterator<Pair<T, Double>> relationIt = list1.iterator();
      Iterator<Pair<T, Double>> relationCandidateSetIt = list2.iterator();
      Pair<T, Double> relationId;
      Pair<T, Double> relationCandidateSetId;
      boolean keepGoing = relationIt.hasNext() && relationCandidateSetIt.hasNext();
      boolean relationItHasNext;
      boolean relationCandidateSetItHasNext;

      if (keepGoing) {

         relationId = relationIt.next();
         relationCandidateSetId = relationCandidateSetIt.next();

         while (keepGoing) {
            relationItHasNext = relationIt.hasNext();
            relationCandidateSetItHasNext = relationCandidateSetIt.hasNext();

            int compareResult = comparator.compare(relationId.first, relationCandidateSetId.first);
            if (compareResult == 0) {

               intersectionSize += Math.min(relationId.second, relationCandidateSetId.second);
               keepGoing = relationItHasNext && relationCandidateSetItHasNext;

               if (keepGoing) {
                  relationId = relationIt.next();
                  relationCandidateSetId = relationCandidateSetIt.next();
               }

            } else if (relationItHasNext && compareResult == -1) {
               relationId = relationIt.next();
            } else if (relationCandidateSetItHasNext) {
               relationCandidateSetId = relationCandidateSetIt.next();
            } else {
               keepGoing = false;
            }

         }
      }

      return intersectionSize;
   }

   public static final <T> double getWeightedListSize(final List<Pair<T, Double>> list) {
      double size = 0.0;
      for (Pair<T, Double> element : list)
         size += element.second;
      return size;
   }

   public static final <T> void normalizeWeightedList(final List<Pair<T, Double>> list, double newSize) {
      double listSize = getWeightedListSize(list);

      for (Pair<T, Double> element : list)
         element.second = element.second / listSize * newSize;
   }
}
