package ca.ulaval.ift.alphonse.filtering;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.pig.FilterFunc;
import org.apache.pig.data.Tuple;

/**
 * Filters out stop words from a bag of chararray strings. Pig UDF.
 * <p>
 * The default words filtered are: "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into",
 * "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this",
 * "to", "was", "will", "with"
 * </p>
 * 
 * @author Mathieu Dumoulin
 */
public class StopWordFilter extends FilterFunc {
   private static final String[] STOP_WORDS = { "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if",
         "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there",
         "these", "they", "this", "to", "was", "will", "with" };

   private static final Set<String> STOP_WORDS_SET = new HashSet<String>();

   static {
      STOP_WORDS_SET.addAll(Arrays.asList(STOP_WORDS));
   }

   @Override
   public Boolean exec(final Tuple input) throws IOException {
      if (input == null || input.size() != 1 || input.isNull(0)) {
         throw new IOException("Unexpected input. Should be a single string (chararray tuple)");
      }

      final String word = (String) input.get(0);

      return STOP_WORDS_SET.contains(word);
   }
}
