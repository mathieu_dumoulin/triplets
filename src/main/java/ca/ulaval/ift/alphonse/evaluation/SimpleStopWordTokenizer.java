/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ulaval.ift.alphonse.evaluation;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;

/**
 * Tokenizes lines of text based on a simple list of common English stopwords (identical to Lucene's default English
 * stop words set. Usage:
 * <p>
 * 
 * <pre>
 * <code>
 * tokenized = FOREACH documents GENERATE doc_id AS doc_id, FLATTEN(SimpleStopWordTokenizer(text)) AS (token:chararray);
 * </code>
 * </pre>
 * 
 * </p>
 * <p>
 * The default words filtered are: "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into",
 * "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this",
 * "to", "was", "will", "with"
 * </p>
 * <p>
 * NOTE: Upgrade to Version.LUCENE_46: Must call tokenStream.reset() before tokenStream.incrementToken() or will result
 * in nullPointerException!
 * </p>
 * <i>Based on code by Jacob Perkins</i>
 * 
 * @see org.apache.lucene.analysis.core.StopAnalyzer
 * @author dumoulma
 */
public class SimpleStopWordTokenizer extends EvalFunc<DataBag> {
   private static final String INPUT_IS_NULL_MSG = "Input tuple contains a null value field!";
   private static final String UNEXPECTED_INPUT_ERROR_MSG = "Unexpected input. Should be chararray (a line of text).";
   private static final int EXPECTED_TUPLE_SIZE = 1;
   private static final int MIN_WORD_LENGTH = 2;
   private static final int MAX_WORD_LENGTH = 17;

   private static final String[] STOPWORDS = { "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if",
         "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there",
         "these", "they", "this", "to", "was", "will", "with", "than" };

   private static BagFactory bagFactory = BagFactory.getInstance();
   private static TupleFactory tupleFactory = TupleFactory.getInstance();

   private static final Set<String> STOPWORDS_SET = new HashSet<String>();

   static {
      STOPWORDS_SET.addAll(Arrays.asList(STOPWORDS));
   }

   @Override
   public DataBag exec(final Tuple input) throws IOException {
      if (input == null || input.size() != EXPECTED_TUPLE_SIZE) {
         throw new IOException(UNEXPECTED_INPUT_ERROR_MSG);
      }
      if (input.isNull(0)) {
         getLogger().debug(INPUT_IS_NULL_MSG);
         return null;
      }

      final String line = input.get(0).toString();
      return addTokensToBag(line);
   }

   private DataBag addTokensToBag(final String line) {
      final DataBag bagOfTokens = bagFactory.newDefaultBag();
      for (final String word : line.split("\\s")) {
         if (!STOPWORDS_SET.contains(word) && StringUtils.isAlphanumeric(word) && word.length() >= MIN_WORD_LENGTH
               && word.length() < MAX_WORD_LENGTH) {
            bagOfTokens.add(tupleFactory.newTuple(word));
         }
      }
      return bagOfTokens;
   }

   @Override
   public Schema outputSchema(final Schema input) {
      Schema outputSchema = null;
      try {
         if (input == null || input.size() != EXPECTED_TUPLE_SIZE) {
            getLogger().warn(UNEXPECTED_INPUT_ERROR_MSG);
         } else if (input.getField(0).type != DataType.CHARARRAY) {
            final String message = UNEXPECTED_INPUT_ERROR_MSG + " Got input type "
                  + DataType.findTypeName(input.getField(0).type);
            getLogger().warn(message);
         }

         final Schema.FieldSchema wordTupleSchema = new Schema.FieldSchema("word", DataType.CHARARRAY);
         final Schema bagSchema = new Schema(wordTupleSchema);
         outputSchema = new Schema(
               new Schema.FieldSchema(getSchemaName("bag_of_words", input), bagSchema, DataType.BAG));
      } catch (final FrontendException e) {
         getLogger().warn(e);
      }
      return outputSchema;
   }
}
