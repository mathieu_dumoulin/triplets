/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ulaval.ift.alphonse.evaluation;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;

/**
 * Tokenizes lines of text using Lucene's StopAnalyzer class. Usage:
 * <p>
 * 
 * <pre>
 * <code>
 * tokenized = FOREACH documents GENERATE doc_id AS doc_id, FLATTEN(LuceneStopWordTokenizer(text)) AS (token:chararray);
 * </code>
 * </pre>
 * 
 * </p>
 * <p>
 * The default words filtered are: "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into",
 * "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this",
 * "to", "was", "will", "with"
 * </p>
 * <p>
 * NOTE: Upgrade to Version.LUCENE_46: Must call tokenStream.reset() before tokenStream.incrementToken() or will result
 * in nullPointerException!
 * </p>
 * <i>Based on code by Jacob Perkins</i>
 * 
 * @see org.apache.lucene.analysis.core.StopAnalyzer
 * @author dumoulma
 */
public class LuceneStopWordTokenizer extends EvalFunc<DataBag> {
   private static final String TOKEN_STREAM_RETURNED_NULL_MSG = "Lucene Analyzer token stream returned null stream!";
   private static final String INPUT_IS_NULL_MSG = "Input has null value field!";
   private static final String UNEXPECTED_INPUT_ERROR_MSG = "Unexpected input. Should be chararray (a line of text).";
   private static final int EXPECTED_TUPLE_SIZE = 1;
   private static final int MIN_WORD_SIZE = 2;

   private static TupleFactory tupleFactory = TupleFactory.getInstance();
   private static BagFactory bagFactory = BagFactory.getInstance();

   private static String noField = "";

   private static StopAnalyzer analyzer = new StopAnalyzer(Version.LUCENE_47);

   @Override
   public DataBag exec(final Tuple input) throws IOException {
      if (input == null || input.size() != EXPECTED_TUPLE_SIZE) {
         throw new IOException(UNEXPECTED_INPUT_ERROR_MSG);
      }
      if (input.isNull(0)) {
         getLogger().warn(INPUT_IS_NULL_MSG);
         return null;
      }

      final String lineOfText = input.get(0).toString();
      final TokenStream stream = analyzer.tokenStream(noField, new StringReader(lineOfText));
      if (stream == null) {
         throw new IOException(TOKEN_STREAM_RETURNED_NULL_MSG);
      }

      return addTokensToBag(stream, stream.addAttribute(CharTermAttribute.class));
   }

   private DataBag addTokensToBag(final TokenStream stream, final CharTermAttribute termAtt) throws IOException {
      final DataBag outputBag = bagFactory.newDefaultBag();
      try {
         stream.reset();
         while (stream.incrementToken()) {
            if (termAtt.length() >= MIN_WORD_SIZE) {
               final Tuple nextWordTuple = tupleFactory.newTuple(termAtt.toString());
               outputBag.add(nextWordTuple);
            }
         }
         stream.end();
      } finally {
         stream.close();
      }
      return outputBag;
   }

   @Override
   public Schema outputSchema(final Schema input) {
      Schema outputSchema = null;
      try {
         if (input == null || input.size() != EXPECTED_TUPLE_SIZE) {
            getLogger().warn(UNEXPECTED_INPUT_ERROR_MSG);
         } else if (input.getField(0).type != DataType.CHARARRAY) {
            final String message = UNEXPECTED_INPUT_ERROR_MSG + " Got input type "
                  + DataType.findTypeName(input.getField(0).type);
            getLogger().warn(message);
         }

         final Schema.FieldSchema wordTupleSchema = new Schema.FieldSchema("word", DataType.CHARARRAY);
         final Schema bagSchema = new Schema(wordTupleSchema);
         outputSchema = new Schema(
               new Schema.FieldSchema(getSchemaName("bag_of_words", input), bagSchema, DataType.BAG));
      } catch (final FrontendException e) {
         getLogger().warn(e);
      }
      return outputSchema;
   }
}
