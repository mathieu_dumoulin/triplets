package ca.ulaval.ift.alphonse.evaluation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.pig.EvalFunc;
import org.apache.pig.PigWarning;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;

import ca.ulaval.ift.alphonse.utility.ListUtils;
import ca.ulaval.ift.alphonse.utility.Pair;

/**
 * Compute the Cosine Similarity between two weighted vectors. Input should be two vectors as bags of tuple where each
 * tuple contains one long field and a double field which is the weight (long, double). Pig UDF evaluation function.
 * 
 * @author Gabriel Massicotte
 */
public class WeightedCosineSimilarity extends EvalFunc<Double> {
   private static final int NB_INNER_FIELDS = 2;

   private static final String SIZE_OF_RELATION_IS_ZERO_MSG = "The size of either relations was 0!";

   private static final int NB_INPUT_FIELDS = 2;

   private final transient Log log = getLogger();

   @Override
   public Double exec(final Tuple input) throws IOException {
      final DataBag relationBag = (DataBag) input.get(0);
      final DataBag candidateRelationBag = (DataBag) input.get(1);

      final List<Pair<Long, Double>> relation = new ArrayList<Pair<Long, Double>>();
      for (final Tuple index : relationBag) {
         relation.add(new Pair<Long, Double>((Long) index.get(0), (Double) index.get(1)));
      }

      final List<Pair<Long, Double>> relationCandidateSet = new ArrayList<Pair<Long, Double>>();
      for (final Tuple index : candidateRelationBag) {
         relationCandidateSet.add(new Pair<Long, Double>((Long) index.get(0), (Double) index.get(1)));
      }

      Comparator<Pair<Long, Double>> c1 = new Comparator<Pair<Long, Double>>() {
         @Override
         public int compare(Pair<Long, Double> o1, Pair<Long, Double> o2) {
            return o1.first.compareTo(o2.first);
         }
      };
      Collections.sort(relation, c1);
      Collections.sort(relationCandidateSet, c1);

      double intersectionSize = ListUtils.getWeightedListIntersectionSize(relation, relationCandidateSet,
            ListUtils.getLongComparator());

      return cosine(ListUtils.getWeightedListSize(relation), ListUtils.getWeightedListSize(relationCandidateSet),
            intersectionSize);
   }
   /**
    * Compute the cosine similarity of two sets. If either A or B are size 0, then their similarity score is 0.0.
    * 
    * @param sizeA
    *           size of set A
    * @param sizeB
    *           size of set B
    * @param sizeInter
    *           size of the intersection of sets A and B
    * @return cosine similarity score
    */
   double cosine(final double sizeA, final double sizeB, final double sizeInter) {
      double similarity = 0.0;
      if (sizeA == 0 || sizeB == 0) {
         warn(SIZE_OF_RELATION_IS_ZERO_MSG, PigWarning.DIVIDE_BY_ZERO);
      } else {
         similarity = sizeInter / Math.sqrt(sizeA) / Math.sqrt(sizeB);// To be sure there are no overflow in calculation
                                                                      // sizeA & sizeB are not multiplied
      }
      return similarity;
   }

   @Override
   public Schema outputSchema(final Schema input) {
      if (input.size() != NB_INPUT_FIELDS) {
         log.warn("Expected (bag:{(index:long, weight:double)}, bag:{(index:long, weight:double)}), input does not have 2 fields");
      }

      final StringBuilder errorMsg = new StringBuilder();
      try {
         if (input.getField(0).type != DataType.BAG || input.getField(1).type != DataType.BAG
               || input.getField(0).schema.size() != NB_INNER_FIELDS
               || input.getField(1).schema.size() != NB_INNER_FIELDS
               || input.getField(0).schema.getField(0).type != DataType.LONG
               || input.getField(1).schema.getField(0).type != DataType.LONG
               || input.getField(0).schema.getField(1).type != DataType.DOUBLE
               || input.getField(1).schema.getField(1).type != DataType.DOUBLE) {
            errorMsg
                  .append("Expected input (bag:{(index:long, weight:double)}, bag:{(index:long, weight:double)}), received schema (");
            errorMsg.append(DataType.findTypeName(input.getField(0).type));
            errorMsg.append(DataType.findTypeName(input.getField(1).type));
            errorMsg.append(')');
         }

      } catch (final FrontendException e) {
         log.warn(errorMsg.toString());
      }

      return new Schema(new FieldSchema(getClass().getName(), DataType.DOUBLE));
   }
}
