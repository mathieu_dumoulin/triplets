package ca.ulaval.ift.alphonse.evaluation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.pig.EvalFunc;
import org.apache.pig.PigWarning;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;

import ca.ulaval.ift.alphonse.utility.ListUtils;

/**
 * Compute the Cosine Similarity between two vectors Input should be two vectors as bags of tuple where each tuple
 * contains one long field. Pig UDF evaluation function.
 * 
 * @author Mathieu Dumoulin
 */
public class CosineSimilarity extends EvalFunc<Double> {
   private static final String SIZE_OF_RELATION_IS_ZERO_MSG = "The size of either relations was 0!";

   private static final int NB_INPUT_FIELDS = 2;

   private final transient Log log = getLogger();

   @Override
   public Double exec(final Tuple input) throws IOException {
      final DataBag relationBag = (DataBag) input.get(0);
      final DataBag candidateRelationBag = (DataBag) input.get(1);

      final List<Long> relation = new ArrayList<Long>();
      for (final Tuple index : relationBag) {
         relation.add((Long) index.get(0));
      }

      final List<Long> relationCandidateSet = new ArrayList<Long>();
      for (final Tuple index : candidateRelationBag) {
         relationCandidateSet.add((Long) index.get(0));
      }

      Collections.sort(relation);
      Collections.sort(relationCandidateSet);

      int intersectionSize = ListUtils.getIntersectionSize(relation, relationCandidateSet,
            ListUtils.getLongComparator());

      return cosine(relation.size(), relationCandidateSet.size(), intersectionSize);
   }
   /**
    * Compute the cosine similarity of two sets. If either A or B are size 0, then their similarity score is 0.0.
    * 
    * @param sizeA
    *           size of set A
    * @param sizeB
    *           size of set B
    * @param sizeInter
    *           size of the intersection of sets A and B
    * @return cosine similarity score
    */
   double cosine(final int sizeA, final int sizeB, final int sizeInter) { // NOPMD by Mathieu Dumoulin on 13/03/14 6:17
      double similarity = 0.0;
      if (sizeA == 0 || sizeB == 0) { // NOPMD by Mathieu Dumoulin on 13/03/14 6:17 PM
         warn(SIZE_OF_RELATION_IS_ZERO_MSG, PigWarning.DIVIDE_BY_ZERO);
      } else {
         similarity = sizeInter / Math.sqrt(sizeA) / Math.sqrt(sizeB);// To be sure there are no overflow in calculation
      }
      return similarity;
   }

   @Override
   public Schema outputSchema(final Schema input) {
      if (input.size() != NB_INPUT_FIELDS) {
         log.warn("Expected (bag:{index:long}, bag:{index:long}), input does not have 2 fields");
      }

      final StringBuilder errorMsg = new StringBuilder();
      try {
         if (input.getField(0).type != DataType.BAG || input.getField(1).type != DataType.BAG) {
            errorMsg.append("Expected input (bag:{index:long}, bag:{index:long}), received schema (");
            errorMsg.append(DataType.findTypeName(input.getField(0).type));
            errorMsg.append(DataType.findTypeName(input.getField(1).type));
            errorMsg.append(')');
         }
      } catch (final FrontendException e) {
         log.warn(errorMsg.toString());
      }

      return new Schema(new FieldSchema(getClass().getName(), DataType.DOUBLE));
   }
}
