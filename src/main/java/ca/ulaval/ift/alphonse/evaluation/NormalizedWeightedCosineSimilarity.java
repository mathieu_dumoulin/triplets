package ca.ulaval.ift.alphonse.evaluation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;

import ca.ulaval.ift.alphonse.utility.ListUtils;
import ca.ulaval.ift.alphonse.utility.Pair;

/**
 * Compute the Cosine Similarity between two normalized weighted vectors. Input should be two vectors as bags of tuple
 * where each tuple contains one long field and a double field which is the weight (long, double). Pig UDF evaluation
 * function.
 * 
 * @author Gabriel Massicotte
 */
public class NormalizedWeightedCosineSimilarity extends EvalFunc<Double> {
   private static final int NB_INNER_FIELDS = 2;

   private static final int NB_INPUT_FIELDS = 2;

   private final transient Log log = getLogger();

   @Override
   public Double exec(final Tuple input) throws IOException {
      final DataBag relationBag = (DataBag) input.get(0);
      final DataBag candidateRelationBag = (DataBag) input.get(1);

      final List<Pair<Long, Double>> relation = new ArrayList<Pair<Long, Double>>();
      for (final Tuple index : relationBag) {
         relation.add(new Pair<Long, Double>((Long) index.get(0), (Double) index.get(1)));
      }

      final List<Pair<Long, Double>> relationCandidateSet = new ArrayList<Pair<Long, Double>>();
      for (final Tuple index : candidateRelationBag) {
         relationCandidateSet.add(new Pair<Long, Double>((Long) index.get(0), (Double) index.get(1)));
      }

      Comparator<Pair<Long, Double>> weightedListComparator = ListUtils.getWeightedLongComparator();
      Collections.sort(relation, weightedListComparator);
      Collections.sort(relationCandidateSet, weightedListComparator);

      ListUtils.normalizeWeightedList(relation, 1);
      ListUtils.normalizeWeightedList(relationCandidateSet, 1);

      double intersectionSize = ListUtils.getWeightedListIntersectionSize(relation, relationCandidateSet,
            ListUtils.getLongComparator());

      // Since the size of the two vectors must be 1 the cosine distance is the intersection size.
      return intersectionSize;
   }

   @Override
   public Schema outputSchema(final Schema input) {
      if (input.size() != NB_INPUT_FIELDS) {
         log.warn("Expected (bag:{(index:long, weight:double)}, bag:{(index:long, weight:double)}), input does not have 2 fields");
      }

      final StringBuilder errorMsg = new StringBuilder();
      try {
         if (input.getField(0).type != DataType.BAG || input.getField(1).type != DataType.BAG
               || input.getField(0).schema.size() != NB_INNER_FIELDS
               || input.getField(1).schema.size() != NB_INNER_FIELDS
               || input.getField(0).schema.getField(0).type != DataType.LONG
               || input.getField(1).schema.getField(0).type != DataType.LONG
               || input.getField(0).schema.getField(1).type != DataType.DOUBLE
               || input.getField(1).schema.getField(1).type != DataType.DOUBLE) {
            errorMsg
                  .append("Expected input (bag:{(index:long, weight:double)}, bag:{(index:long, weight:double)}), received schema (");
            errorMsg.append(DataType.findTypeName(input.getField(0).type));
            errorMsg.append(DataType.findTypeName(input.getField(1).type));
            errorMsg.append(')');
         }

      } catch (final FrontendException e) {
         log.warn(errorMsg.toString());
      }

      return new Schema(new FieldSchema(getClass().getName(), DataType.DOUBLE));
   }
}
