#!/bin/bash



MIN_SIMILARITY=0.2
TOP_K_RELATIONS=100


FOLDER=data/streamOuts/
OUT=${FOLDER}triplets-$1-$3-$(date +%Y%m%d%H%M%S).out
ERR=${FOLDER}triplets-$1-$3-$(date +%Y%m%d%H%M%S).err

FILE_NAME_SUFFIX="-$1-$3$4"
TIME_STAMP=" $4"

MAPRED_DIR=data/temp/$FILE_NAME_SUFFIX/
mkdir $MAPRED_DIR -p
mkdir ${MAPRED_DIR}local -p

PARAM_FILE_SEED="scripts/params"$FILE_NAME_SUFFIX".param"
SUBMISSION_FILE="scripts/submit"$FILE_NAME_SUFFIX".sh"
TIME_FILE=${FOLDER}"time-"$1"-"$3".out"
PIG_COMMAND="pig -Dpig.temp.dir=${MAPRED_DIR} -Dmapred.local.dir=${MAPRED_DIR}local -Dio.sort.factor=10 -Dmapred.child.java.opts=-Xmx2G -Dio.sort.mb=500 -m "
TIME_COMMAND="\$(date \"+%Y/%m/%d %H:%M:%S\")"


#Generates parameters file

OFFSET=21000
INC=1000
LIMIT=$(($OFFSET + 1*$INC))
for (( c=$OFFSET; c<$LIMIT; c=$c+$INC))
do
	PARAM_FILE=${PARAM_FILE_SEED}_${c}

	rm -f $PARAM_FILE

	echo "LIB_DIR=lib" >> $PARAM_FILE
	echo "DEFAULT_PARALLEL="$(($3*6)) >> $PARAM_FILE
	echo "COMPRESSION_CODEC=gz" >> $PARAM_FILE
	echo "MIN_SIMILARITY="${MIN_SIMILARITY} >> $PARAM_FILE
	echo "TOP_K_RELATIONS="${TOP_K_RELATIONS} >> $PARAM_FILE
	echo "FIRST_RELATION_ID="$c >> $PARAM_FILE
	echo "LAST_RELATION_ID="$(($c+$INC)) >> $PARAM_FILE
	
	echo "RELATION_TRIPLETS=data/wiki-relations/out"$1".tsv" >> $PARAM_FILE
	echo "WORDNET_SYNSETS=data/wordnet/wordnet_sampleALL.tsv" >> $PARAM_FILE
	
	echo "RELATIONS_DICT=data/output/relations"$FILE_NAME_SUFFIX >> $PARAM_FILE
	echo "VOCAB_DICT=data/output/vocab"$FILE_NAME_SUFFIX >> $PARAM_FILE
	echo "CONTEXT_VECTORS=data/output/cv"$FILE_NAME_SUFFIX >> $PARAM_FILE
	
	echo "PREDICATE_RELATIONS=data/output/predicate-relations"$FILE_NAME_SUFFIX >> $PARAM_FILE
	echo "SYNSET_CANDIDATE_GROUPS_IDS=data/output/synset-candidate-groups-ids"$FILE_NAME_SUFFIX >> $PARAM_FILE
	echo "SYNSET_CANDIDATE_GROUPS_DICTIONARY=data/output/synset-candidate-groups-dict"$FILE_NAME_SUFFIX >> $PARAM_FILE
	
	echo "CANDIDATE_RELATIONS=data/output/candidates"$FILE_NAME_SUFFIX >> $PARAM_FILE
	echo "COSINE_DISTANCES_IN=data/output/simil"$FILE_NAME_SUFFIX"_"${c} >> $PARAM_FILE
	echo "COSINE_DISTANCES_OUT=data/output/simil"$FILE_NAME_SUFFIX"_"$((${c}+${INC})) >> $PARAM_FILE
	echo "SIMILAR_RELATIONS=data/output/relation-simil"$FILE_NAME_SUFFIX >> $PARAM_FILE
done


#Generates submission script

echo "" >> $TIME_FILE
rm -f $SUBMISSION_FILE

echo "#! /bin/bash" >> $SUBMISSION_FILE
echo "#PBS -A bue-543-aa" >> $SUBMISSION_FILE
echo "#PBS -N simil-"$1 >> $SUBMISSION_FILE
echo "#PBS -l walltime="$2 >> $SUBMISSION_FILE
echo "#PBS -l nodes="$3":ppn=8" >> $SUBMISSION_FILE
echo "cd \${PBS_O_WORKDIR}" >> $SUBMISSION_FILE
echo "module load apps/hadoop/1.2.0 apps/pig/0.12.0" >> $SUBMISSION_FILE
echo "module load compilers/intel/2013 mpi/openmpi/1.6.4_intel" >> $SUBMISSION_FILE
echo "source /clumeq/bin/cq_hadoop_1.2.0.sh" >> $SUBMISSION_FILE
echo "setup_hadoop" >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE

echo "echo \"Start time : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/make-relation-dict.pig" >> $SUBMISSION_FILE
echo "echo \"Relation-dict creation time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE

#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/make-vocab-dict.pig" >> $SUBMISSION_FILE
echo "echo \"Vocab-dict creation time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE

#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/make-context-vectors.pig" >> $SUBMISSION_FILE
echo "echo \"Context-vector creation time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE

#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/make-candidates-groups-for-synsets.pig" >> $SUBMISSION_FILE
#echo "echo \"Candidates-groups creation time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
#echo "" >> $SUBMISSION_FILE

#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/selectCandidates.pig" >> $SUBMISSION_FILE
#echo "echo \"Candidate selection time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
#echo "" >> $SUBMISSION_FILE

#echo ${PIG_COMMAND}${PARAM_FILE_SEED}"_"$OFFSET" pig/cosine"$4".pig" >> $SUBMISSION_FILE
echo "echo \"Similarity calculation time STEP_"${OFFSET}${TIME_STAMP}" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
echo "" >> $SUBMISSION_FILE


for (( c=$OFFSET+$INC; c<$LIMIT; c=$c+$INC ))
do
	PARAM_FILE=${PARAM_FILE_SEED}_${c}

	#echo ${PIG_COMMAND}$PARAM_FILE" pig/cosine-append.pig" >> $SUBMISSION_FILE
	echo "echo \"Similarity calculation time STEP_"${c}$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE
	echo "" >> $SUBMISSION_FILE
done


echo ${PIG_COMMAND}${PARAM_FILE}" pig/index2rel.pig" >> $SUBMISSION_FILE
echo "echo \"Id replacement time"$TIME_STAMP" : \"${TIME_COMMAND} >> "$TIME_FILE >> $SUBMISSION_FILE


#Submits the script

msub $SUBMISSION_FILE -e $ERR -o $OUT