REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/guava-16.0.1.jar
REGISTER $LIB_DIR/commons-lang3-3.2.1.jar

DEFINE COSINE ca.ulaval.ift.alphonse.evaluation.CosineSimilarity();       

SET job.name 'triplets_with_index';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec $COMPRESSION_CODEC;


relations_joined_tmp = LOAD '$CANDIDATE_RELATIONS'
    AS (predicate_relation_index:long, candidate_relation_index:long);


-- cosine similarity measure is symetric, so we will only
-- compute it in one direction, enforced by this condition
candidate_relations = FILTER relations_joined_tmp 
    BY predicate_relation_index > candidate_relation_index;    

-- load the pre-computed context vectors
context_vectors = LOAD '$CONTEXT_VECTORS' 
    AS (relation_index:long, context_vector:bag{(word_index:long)});

-- join the context vectors with the predicate relations and the
-- candidate relations from above
predicate_relations_with_context_vectors_tmp = 
    JOIN candidate_relations BY predicate_relation_index,
    context_vectors BY relation_index; -- USING 'replicated';    

pr_context_vectors = 
    FOREACH predicate_relations_with_context_vectors_tmp GENERATE
    predicate_relation_index AS predicate_relation_index,
    context_vectors::context_vector AS predicate_context_vector,
    candidate_relation_index AS candidate_relation_index;

relations_with_context_vectors_tmp = 
    JOIN pr_context_vectors BY candidate_relation_index,
    context_vectors BY relation_index; -- USING 'replicated';    

relations_with_context_vectors = 
    FOREACH relations_with_context_vectors_tmp GENERATE
    predicate_relation_index AS predicate_relation_index,
    predicate_context_vector AS predicate_context_vector,
    candidate_relation_index AS candidate_relation_index,
    context_vector AS candidate_context_vector;   
 
 -- appliquer le cosine similarity sur les vecteurs de contextes pour chaque
-- paire de predicate_relation et candidate_relation
predicate_relations_scored = 
    FOREACH relations_with_context_vectors 
    GENERATE predicate_relation_index, candidate_relation_index, 
    COSINE(predicate_context_vector, candidate_context_vector) 
        AS similarity_score;

predicate_relations_scored_filtered = FILTER predicate_relations_scored 
    BY similarity_score > $MIN_SIMILARITY; 

predicate_relations_scored_grouped = 
    GROUP predicate_relations_scored_filtered BY predicate_relation_index;

    
relations_by_ranked_similarities = FOREACH predicate_relations_scored_grouped {
    relations_scored = FOREACH predicate_relations_scored_filtered 
                    GENERATE candidate_relation_index, similarity_score;                     
    --relations_scored = ORDER relations_scored BY similarity_score DESC;    
    relations_limited = LIMIT relations_scored $TOP_K_RELATIONS;
    GENERATE group AS predicate_relation_index, relations_limited AS similar_relations;
}

relations_by_ranked_similarities = FOREACH relations_by_ranked_similarities 
    GENERATE predicate_relation_index, FLATTEN(similar_relations);

rmf $COSINE_DISTANCES;
STORE relations_by_ranked_similarities INTO '$COSINE_DISTANCES';
