REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/guava-16.0.1.jar
REGISTER $LIB_DIR/commons-lang3-3.2.1.jar
REGISTER $LIB_DIR/datafu-1.2.0.jar

SET job.name 'triplets_with_index';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec $COMPRESSION_CODEC;

DEFINE ENUMERATE datafu.pig.bags.Enumerate();

candidates_groups_dict = LOAD '$SYNSET_CANDIDATE_GROUPS_DICTIONARY' AS (candidate_group:{(candidate_relation_id:long)},candidate_group_id:long);
candidates_groups_ids = LOAD '$SYNSET_CANDIDATE_GROUPS_IDS' AS (synset_id:long,candidate_group_id:long);
predicate_relations = LOAD '$PREDICATE_RELATIONS' AS (predicate_relation_id:long, synset_id:long);




-- FOR TESTING: count the relations before they are joined
--predicate_count = FOREACH (GROUP predicate_relations ALL) GENERATE COUNT($1);
--candidate_count = FOREACH (GROUP candidate_relations_grouped_by_id ALL) GENERATE COUNT($1);

-- this is the new big join of relations with other relations BY wordnetID
-- 'replicated' is a faster join that holds the second relation in memory
-- allowing the join to be done much faster. order is important! big THEN small!
relations_joined = JOIN predicate_relations BY synset_id,
    candidates_groups_ids BY synset_id;

--keep only relevant columns
relations_joined = FOREACH relations_joined GENERATE predicate_relation_id, candidate_group_id;
relations_joined = DISTINCT relations_joined;


-- Generates a dictionary linking to the sets of the ids of the sets linking to the relation's ids
-- (It is necessary to merge the sets linking to the relation's ids before the final join to eliminate duplicates before this last state)
relations_groups_groups_ids = GROUP relations_joined BY predicate_relation_id;
relations_groups_groups_ids = FOREACH relations_groups_groups_ids
    GENERATE group AS predicate_relation_id,
    relations_joined.candidate_group_id AS candidate_group_id,
    BagToString(relations_joined.candidate_group_id) AS key;

relations_groups_groups_dictionary = FOREACH relations_groups_groups_ids 
    GENERATE key, candidate_group_id;

-- DISTINCT ON BAG
relations_groups_groups_dictionary = GROUP relations_groups_groups_dictionary BY key;
relations_groups_groups_dictionary = FOREACH relations_groups_groups_dictionary {
    entry = LIMIT relations_groups_groups_dictionary.candidate_group_id 1;
    GENERATE group AS key, entry AS candidate_relation_indexes;
}

relations_groups_groups_dictionary = FOREACH (GROUP relations_groups_groups_dictionary ALL)
    GENERATE FLATTEN(ENUMERATE(relations_groups_groups_dictionary)) AS (dict_key, candidate_relation_index_set_index, index:long);

relations_groups_groups_dictionary_light = FOREACH relations_groups_groups_dictionary
    GENERATE FLATTEN(candidate_relation_index_set_index) AS candidate_relation_index_set_index, index;



-- Generate the "relations_joined" table using the "set_id"
predicate_groups_groups_ids = JOIN relations_groups_groups_ids BY key,
    relations_groups_groups_dictionary BY dict_key;
predicate_groups_groups_ids = FOREACH predicate_groups_groups_ids 
    GENERATE predicate_relation_id, index AS candidate_group_id;


relations_groups_groups_dictionary_light = FOREACH relations_groups_groups_dictionary_light
    GENERATE FLATTEN(candidate_relation_index_set_index) AS candidate_relation_index_set_index, index;
relations_groups_groups_dictionary_light = JOIN relations_groups_groups_dictionary_light BY candidate_relation_index_set_index,
    candidates_groups_dict BY candidate_group_id;
relations_groups_groups_dictionary_light = FOREACH relations_groups_groups_dictionary_light
    GENERATE index, candidate_group;
relations_groups_groups_dictionary_light = GROUP relations_groups_groups_dictionary_light BY index;
relations_groups_dictionary_light = FOREACH relations_groups_groups_dictionary_light {
    entries = FOREACH relations_groups_groups_dictionary_light.candidate_group
	GENERATE FLATTEN(candidate_group);
    entries = DISTINCT entries;
    GENERATE group AS candidate_group_id, entries AS candidate_relation_ids;
}


candidate_groups = JOIN predicate_groups_groups_ids BY candidate_group_id,
        relations_groups_dictionary_light BY candidate_group_id;
predicate_candidate_pairs = FOREACH candidate_groups
    GENERATE predicate_relation_id,
    FLATTEN(candidate_relation_ids) AS candidate_relation_id;


rmf $CANDIDATE_RELATIONS;
STORE predicate_candidate_pairs INTO '$CANDIDATE_RELATIONS';