REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/datafu-1.2.0.jar

DEFINE ENUMERATE datafu.pig.bags.Enumerate();

SET job.name 'Make_relation_dict';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec gz;

triplets = LOAD '$RELATION_TRIPLETS'
    AS (left_context_words:chararray,relation:chararray,right_context_words:chararray);

-- faire la liste de toutes les relations differentes
relations = foreach triplets generate relation;   
relations =  FOREACH (GROUP relations BY relation) GENERATE group as relation, COUNT($1);

-- ENNUMERATE needs a bag as input
-- transform the list of relations into a bag
relations_bag = FOREACH (GROUP relations ALL) {
    rels = relations.relation;
    GENERATE rels as relations;
}

dictionnary = FOREACH relations_bag 
    GENERATE FLATTEN(ENUMERATE(relations)) as(relation, index:long);

dictionnary_filtered = FILTER dictionnary BY 
    SIZE(relation) > 1 AND relation MATCHES '\\w.*';
 
rmf $RELATIONS_DICT;
STORE dictionnary_filtered INTO '$RELATIONS_DICT';