REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/guava-16.0.1.jar

DEFINE COSINE ca.ulaval.ift.alphonse.evaluation.CosineSimilarity();
DEFINE StopTokenizer ca.ulaval.ift.alphonse.evaluation.SimpleStopWordTokenizer();          

SET job.name 'Index_to_relation';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec gz;

relations_dictionary = LOAD '$RELATIONS_DICT' AS (relation:chararray,relation_index:long);
  
-- this is the result from triplets-indexed.pig
scored_relations = LOAD '$COSINE_DISTANCES' 
    AS (predicate_relation_index:long, 
    candidate_relation_index:long, 
    similarity_score: double);

predicate_replaced_tmp = JOIN scored_relations BY predicate_relation_index,
    relations_dictionary BY relation_index;

predicate_replaced = FOREACH predicate_replaced_tmp 
    GENERATE relation as predicate_relation, candidate_relation_index, similarity_score;

candidate_replaced_tmp = JOIN predicate_replaced BY candidate_relation_index,
    relations_dictionary BY relation_index;
    
candidate_replaced = FOREACH candidate_replaced_tmp 
    GENERATE predicate_relation, relation as candidate_relation, similarity_score;

grouped = GROUP candidate_replaced BY predicate_relation;

out = FOREACH grouped {
    scored_candidates = FOREACH candidate_replaced GENERATE candidate_relation, similarity_score;
    scored_candidates = ORDER scored_candidates BY similarity_score DESC; 
    GENERATE group as predicate_relation, scored_candidates;
}

rmf $SIMILAR_RELATIONS;
STORE out INTO '$SIMILAR_RELATIONS';
