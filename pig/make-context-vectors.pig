REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/commons-lang3-3.2.1.jar

DEFINE StopTokenizer ca.ulaval.ift.alphonse.evaluation.SimpleStopWordTokenizer();          

SET job.name 'Make_context_vectors';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec gz;

-- load the vocabulary index
vocab_dictionary = LOAD '$VOCAB_DICT' 
    AS (word:chararray, word_index:long);

relations_dictionary = LOAD '$RELATIONS_DICT' 
    AS (relation:chararray, relation_index:long);

triplets = LOAD '$RELATION_TRIPLETS' AS (left_context_words:chararray,
                                      relation:chararray,
                                      right_context_words:chararray);

-- now we want to make context vectors: bags of word index
-- from the words that surround each relation in order to 
-- eventually compute the similarity scores.
-- NOTE: the similarity will compare the context vectors of pairs of relations
-- we got in candidate_relations.
relations_with_left_context_words = foreach triplets 
    generate relation as predicate_relation, 
        FLATTEN(StopTokenizer(left_context_words)) as context_word:chararray;
relations_with_right_context_words = foreach triplets 
    generate relation as predicate_relation, 
        FLATTEN(StopTokenizer(right_context_words)) as context_word:chararray;
relations_with_context_words = 
    UNION relations_with_left_context_words, relations_with_right_context_words;
relations_with_context_words = DISTINCT relations_with_context_words;
    
-- change the relation key with its relation index from the relation dictionnary
contexts_words_tmp = JOIN relations_with_context_words BY predicate_relation,
     relations_dictionary by relation;
contexts_words = FOREACH contexts_words_tmp 
    GENERATE relation_index as relation_index, context_word as context_word;

-- the vectors come from joining the words of the context of each relation
-- with the dictionary we created above
context_words_indexed = JOIN contexts_words by context_word,
         vocab_dictionary by word;

-- We want to fix up the relation so that each relation index has
-- a bag of word index values, so we need to drop unnecessary columns
relation_context_vectors_grouped = 
    GROUP context_words_indexed by relation_index;   
relation_context_vectors = FOREACH relation_context_vectors_grouped {                                    
        indices = FOREACH context_words_indexed generate word_index;
        indices = DISTINCT indices;
        GENERATE group as relation_index, indices as context_vector;
        }
rmf $CONTEXT_VECTORS;
STORE relation_context_vectors INTO '$CONTEXT_VECTORS';