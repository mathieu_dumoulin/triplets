REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/guava-16.0.1.jar
REGISTER $LIB_DIR/commons-lang3-3.2.1.jar
REGISTER $LIB_DIR/datafu-1.2.0.jar

DEFINE ENUMERATE datafu.pig.bags.Enumerate();
DEFINE StopTokenizer ca.ulaval.ift.alphonse.evaluation.SimpleStopWordTokenizer();          

SET job.name 'triplets_with_index';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec $COMPRESSION_CODEC;

relations_dictionary = LOAD '$RELATIONS_DICT' AS (relation:chararray,index:long);

triplets = LOAD '$RELATION_TRIPLETS'
    AS (left_context_words:chararray,relation:chararray,right_context_words:chararray);

-- for testing purposes, limit the number of relations
-- relations_dictionary = FILTER relations_dictionary BY relation MATCHES '.*information.*'; 

-- chaque relation (identifiee par son index) est associee aux mots qui la
-- compose, moins les stop words
tokenized_relations = foreach relations_dictionary 
               generate index, 
               FLATTEN(StopTokenizer(relation)) AS relation_word:chararray;
tokenized_relations_count = FOREACH (GROUP tokenized_relations ALL) GENERATE COUNT($1);

                    
wordnet = load '$WORDNET_SYNSETS' 
    AS (id:long, wid:int, word:chararray, sstype:chararray, 
    sensenum:int, tagcount:int);

-- On enleve les colonnes inutiles de wordnet, on garde id et word
wordnet_filtered = FOREACH wordnet GENERATE id, word;

-- get the id of the words of the relations
-- be list of, topkrelations::be, wordnet::be, ### (wordnet ID de be)
-- on peut avoir plusieurs iD differents pour un mot donne (a cause des synset id)
relation_words_joined_with_wordnet = 
    JOIN tokenized_relations BY relation_word, wordnet_filtered BY word;

-- renommage et selection des colonnes qui nous interessent
-- c-a-d relation_index et synset_id
predicate_relations = FOREACH relation_words_joined_with_wordnet 
    GENERATE index AS predicate_relation_index, id AS synset_id;

rmf $PREDICATE_RELATIONS;
STORE predicate_relations INTO '$PREDICATE_RELATIONS';

candidate_relations_tmp = FOREACH relation_words_joined_with_wordnet 
    GENERATE index AS candidate_relation_index, id AS synset_id;

-- optimisation to reduce the number of items to be joined.
candidate_relations_grouped_by_id = GROUP candidate_relations_tmp BY synset_id;

candidate_relations_grouped_by_id = FOREACH candidate_relations_grouped_by_id 
    GENERATE group AS synset_id, 
    candidate_relations_tmp.candidate_relation_index AS candidate_relation_indexes;



new_candidate_relations_grouped_by_id = FOREACH candidate_relations_grouped_by_id 
    GENERATE synset_id, candidate_relation_indexes, BagToString(candidate_relation_indexes) AS key;


--Makes a dictionnary of the sets to reduce the space taken by the next join on relations
set_dictionnary = FOREACH new_candidate_relations_grouped_by_id 
    GENERATE key, candidate_relation_indexes;

-- DISTINCT ON BAG
set_dictionnary = GROUP set_dictionnary BY key;
set_dictionnary = FOREACH set_dictionnary {
    entry = LIMIT set_dictionnary.candidate_relation_indexes 1;
    GENERATE group AS key, entry AS candidate_relation_indexes;
}

set_dictionnary = FOREACH (GROUP set_dictionnary ALL)
    GENERATE FLATTEN(ENUMERATE(set_dictionnary)) as(dict_key, candidate_relation_index_set, index:long);

set_dictionnary_light = FOREACH set_dictionnary
    GENERATE FLATTEN(candidate_relation_index_set) AS candidate_relation_index_set, index;
rmf $SYNSET_CANDIDATE_GROUPS_DICTIONARY;
STORE set_dictionnary_light INTO '$SYNSET_CANDIDATE_GROUPS_DICTIONARY';

candidates_groups_ids = JOIN new_candidate_relations_grouped_by_id BY key,
    set_dictionnary BY dict_key;
candidates_groups_ids = FOREACH candidates_groups_ids 
    GENERATE synset_id AS synset_id, index AS candidate_group_id;

-- DISTINCT ON BAG (Selects valuable synset ids : those bringing new information about what are the possible candidates)
candidates_groups_ids = GROUP candidates_groups_ids BY candidate_group_id;
candidates_groups_ids = FOREACH candidates_groups_ids {
    entry = LIMIT candidates_groups_ids.synset_id 1;
    GENERATE entry AS synset_id, group AS candidate_group_id;
}
candidates_groups_ids = FOREACH candidates_groups_ids GENERATE FLATTEN(synset_id) AS synset_id, candidate_group_id;

rmf $SYNSET_CANDIDATE_GROUPS_IDS;
STORE candidates_groups_ids INTO '$SYNSET_CANDIDATE_GROUPS_IDS';