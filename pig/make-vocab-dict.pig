REGISTER $LIB_DIR/alphonse-0.0.3.jar
REGISTER $LIB_DIR/datafu-1.2.0.jar
REGISTER $LIB_DIR/commons-lang3-3.2.1.jar

DEFINE ENUMERATE datafu.pig.bags.Enumerate();
DEFINE StopTokenizer ca.ulaval.ift.alphonse.evaluation.SimpleStopWordTokenizer(); 

SET job.name 'Make_vocab_dict';
SET default_parallel $DEFAULT_PARALLEL;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec gz;

triplets = LOAD '$RELATION_TRIPLETS'
    AS (left_context_words:chararray,relation:chararray,right_context_words:chararray);

-- now we want to make vectors from the words of the context
-- to compute similarity scores      
left_context_words = foreach triplets 
    generate FLATTEN(StopTokenizer(left_context_words)) as context_word:chararray;


right_context_words = foreach triplets 
    generate FLATTEN(StopTokenizer(right_context_words)) as context_word:chararray;


context_words = UNION left_context_words, right_context_words;
context_words = DISTINCT context_words;

filtered_context_words = FILTER context_words BY context_word MATCHES '\\w{2,}';

vocabulary = GROUP filtered_context_words ALL;

dictionnary = FOREACH vocabulary GENERATE FLATTEN(ENUMERATE(filtered_context_words)) as(word, index:long);

rmf $VOCAB_DICT;
STORE dictionnary INTO '$VOCAB_DICT';