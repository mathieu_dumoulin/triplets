REGISTER $LIB_DIR/alphonse-*.jar

DEFINE COSINE ca.ulaval.ift.alphonse.evaluation.CosineSimilarity();

relations_with_context_vectors = load 'input' using PigStorage() 
    as();
predicate_relations_scored = 
    FOREACH relations_with_context_vectors 
    GENERATE predicate_relation, candidate_relation, 
    COSINE(predicate_relation_vector, candidate_relation_vector) 
        as similarity_score;