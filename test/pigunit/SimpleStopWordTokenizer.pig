REGISTER lib/alphonse-*.jar

DEFINE StopTokenizer ca.ulaval.ift.alphonse.evaluation.SimpleStopWordTokenizer(); 

text = LOAD 'data/test/lines.txt' as (line:chararray);
tokens = FOREACH text GENERATE line, FLATTEN(StopTokenizer(line)) as word:chararray;