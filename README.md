Pour lancer l'interpreteur interactif pig (grunt)

pig -x local

pour lancer le script, a partir de la racine lancer pig
de grunt:
run -param_file pig/triplets2.param pig/triplets.pig
run -param_file pig/triplets2.param pig/triplets_part.pig
run -param_file pig/tripletsALL.param pig/triplets_part.pig

--evite java.lang.OutOfMemoryError: Java heap space (execmode: -x local)
set mapred.child.java.opts=-Xmx2048m
et/ou 
	sudo nano /etc/hadoop/hadoop-env.sh
	export HADOOP_CLIENT_OPTS="-Xmx2048m $HADOOP_CLIENT_OPTS"

--output path : data/execution_results


--------------séparation en petits fichiers-----------
--/home/massga/git2/triplets/candidate_relations.txt
--mkdir fileParts
--split -l 10000 part-r-00000 fileParts/candidate_relations
-----------------------------------------------------